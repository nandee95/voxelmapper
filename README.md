**VoxelMapper** is a **3D scanner robot** using **Kinect v1** and **Raspberry Pi 2B SBC**. This program runs on a remote computer and processes all the information the robot collects and sends to this program through sockets.

The code for the robot itself and more info about the hardware can be found in a different repo [HERE](https://gitlab.com/nandee95/voxel-mapper-onboard).

This program was developed for **Windows** using **VS2019** and uses **CMake** & **C++17** standard but it may work with older versions! See **BUILDING.md** for build instructions.

**Features**
- Building a 3D map from depth data, position and orientation
- Navigating the 3D map \
TODO:
- OBJ export
- Measurements in 3D space

**Screenshots** \
TODO

**Technical details** \
The robot is able to capture depth frames from the kinect ~20frames/seconds (due to hardware limitations) after each successful capture its wakes up a thread to start compression. The compression takes usually ~100ms when it's done it packs the compressed frame and sensor data into a single packet and sends it to this program. This program decompresses the frame (takes ~4ms on a modern hardware) than uploads it to the GPU for further processing. DepthMapProjection.cl threats every pixel in the depth map as a ray with depth length than calculates world cordinates than grid coordinates. The output is a point coloud of 4m x 4m x 1m, where 0 means there is no changes made, 1 means the point is a block, -1 means the point is air.