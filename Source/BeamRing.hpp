#pragma once

#include "Transformable.hpp"
#include "Colorable.hpp"
#include "VertexArray.hpp"
#include "Shader.hpp"
#include "TempEntity.hpp"
#include <chrono>

class BeamRing : public Transformable, public Colorable, public TempEntity
{
	static std::shared_ptr < VertexArray> va;
	static std::shared_ptr<Shader> shader;
	static int32_t count;
	std::chrono::time_point<std::chrono::steady_clock> start;
	float duration = 2000;
	float radius = 5000;
	float height = 100;
public:
	BeamRing() : start(std::chrono::high_resolution_clock::now())
	{
	}

	void Reset(float radius = -1, float duration = -1)
	{
		if (radius > 0) this->radius = radius;
		if (duration > 0) this->duration = duration;
		start = std::chrono::high_resolution_clock::now();
	}

	static void LoadResources()
	{
		shader = Shader::LoadFromMemory({
			"BeamRing",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec3 aPos; \n
layout(location = 1) uniform mat4 projection; \n
layout(location = 2) uniform mat4 view; \n
layout(location = 3) uniform float height; \n
layout(location = 4) uniform float radius; \n
layout(location = 5) out float amplitude; \n
layout(location = 7) uniform float progress; \n
layout(location = 8) uniform mat4 model; \n
void main() {
\n
gl_Position = projection * view * model * vec4(aPos.xy * radius * progress, aPos.z * height, 1); \n
amplitude = 1 - aPos.z; \n
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor; \n
layout(location = 5) in float amplitude;
layout(location = 6) uniform vec4 color;
layout(location = 7) uniform float progress;
void main()\n
{ \n
	FragColor = vec4(color.rgb,color.a * amplitude * (1 - progress)); \n
}\n
#endif
			)
			});
		std::vector<glm::vec3> vtx;
		constexpr float step = glm::two_pi<float>() / 128.f;
		for (float i = 0; i < glm::two_pi<float>(); i += step)
		{
			const glm::vec3 pts[] = {
				{cos(i),sin(i),0},
				{cos(i + step) ,sin(i + step),0},
			};
			vtx.push_back(pts[0]);
			vtx.push_back(pts[1]);
			vtx.push_back(pts[0] + glm::vec3(0, 0, 1));

			vtx.push_back(pts[0] + glm::vec3(0, 0, 1));
			vtx.push_back(pts[1] + glm::vec3(0, 0, 1));
			vtx.push_back(pts[1]);
		}
		count = vtx.size();
		va = std::make_shared < VertexArray>();
		va->FillVertices(vtx);
		va->SetAttribPointers({ {3,GL_FLOAT} });
	}


	bool Render(const FrameInfo& info)
	{
		float elapsed = (std::chrono::high_resolution_clock::now() - start).count() / 1000000.f;
		if (elapsed > duration) return false;
		glDisable(GL_CULL_FACE);
		shader->Use();
		shader->SetUniform(1, info.projection);
		shader->SetUniform(2, info.view);
		shader->SetUniform(3, height);
		shader->SetUniform(4, radius);
		shader->SetUniform(6, color);
		shader->SetUniform(7, (elapsed / duration));
		shader->SetUniform(8, GetTransform());
		va->DrawArrays(GL_TRIANGLES, 0, count);
		glEnable(GL_CULL_FACE);
		return true;
	}

	void SetHeight(const float& height)
	{
		this->height = height;
	}

	const float& GetHeight() const
	{
		return height;
	}

	void SetDuration(const float& duration)
	{
		this->duration = duration;
	}

	const float& GetDuration() const
	{
		return duration;
	}

	void SetRadius(const float& radius)
	{
		this->radius = radius;
	}

	const float GetRadius() const
	{
		return radius;
	}

};

std::shared_ptr < VertexArray> BeamRing::va;
std::shared_ptr<Shader> BeamRing::shader;
int32_t BeamRing::count;