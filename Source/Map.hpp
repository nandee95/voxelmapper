#pragma once
#include <algorithm>
#include <thread>
#include <array>
#include <vector>
#include <map>
#include <optional>
#include <glm/vec3.hpp>
#include "VertexArray.hpp"
#include "Transformable.hpp"
#include "Chunk.hpp"
#include "OpenCL.hpp"
#include "Globals.hpp"
#include "Entity.hpp"

#include <functional>

namespace std
{
	template<> struct less<glm::ivec2>
	{
		bool operator() (const glm::ivec2& lhs, const glm::ivec2& rhs) const
		{
			return lhs.y*10000 + lhs.x < rhs.y*10000 + rhs.x;
		}
	};
}

class Map : public Entity
{
	std::shared_ptr<Shader> chunkShader,projShader;
	std::map<glm::ivec2,std::shared_ptr<Chunk>> chunks;

	ocl::Kernel projectionKernel, mergerKernel;
	std::shared_ptr<ocl::Buffer> depthMapBuf, depthMapResBuf, fieldBuf, angleBuf, positionBuf, moddedChunksBuf,kernelInfoBuf;
	ocl::CmdQueue queue;

	struct KernelInfo
	{
		float gridResolution = Chunk::resolution;
		cl_float3 gridSize{Chunk::dimensions.x,Chunk::dimensions.y,Chunk::dimensions.z};

		cl_float3 fieldSize  {
			4 * Chunk::dimensions.x,
			4 * Chunk::dimensions.y,
			Chunk::dimensions.z
		};

		cl_float3 fieldCenter{fieldSize.x /2,fieldSize.y/2,0};

		cl_float2 kinectFOV{ Robot::kinectFOV.x,Robot::kinectFOV.y };
		cl_float3 kinectOffset{ Robot::kinectOffset.x,Robot::kinectOffset.y,Robot::kinectOffset.z };
	} kernelInfo;

	std::vector<cl_int> emptyField;
public:
	Map() : queue(Globals::oclContext)
	{
		emptyField.resize(kernelInfo.fieldSize.x* kernelInfo.fieldSize.y* kernelInfo.fieldSize.z);
		InitKernel();
	}

	virtual void LoadResources()
	{
		chunkShader = chunkShader->LoadFromMemory({
			"Chunk",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec4 aPos; \n
layout(location = 1) in vec4 aNorm; \n
layout(location = 0) uniform mat4 projection; \n
layout(location = 1) uniform mat4 view; \n
layout(location = 2) uniform mat4 model; \n
layout(location = 3) out vec3 norm; \n
void main() {
\n
gl_Position = projection * view * model * vec4(aPos.xyz, 1.0); \n
norm = aNorm.xyz; \n
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor; \n
layout(location = 3) in vec3 norm; \n
layout(location = 3) uniform vec3 color; \n
void main()\n
{ \n
float shading = dot(norm, vec3(1, 1, 1)) / 10.f;
shading = 0.3 + 0.7 * shading; \n
FragColor = vec4(color * shading, 1); \n
}\n
#endif
			)
			});

		projShader = chunkShader->LoadFromMemory({
			"ChunkProjection",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec3 aPos; \n
layout(location = 1) in vec3 aColor; \n
layout(location = 0) uniform mat4 projection; \n
layout(location = 1) uniform mat4 view; \n
layout(location = 2) uniform mat4 model; \n
layout(location = 3) out vec3 color; \n
void main() {
\n
gl_Position = projection * view * model * vec4(aPos, 1.0); \n
color = aColor; \n
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor; \n
layout(location = 3) in vec3 color; \n
void main()\n
{ \n
FragColor = vec4(color , 1); \n
}\n
#endif
			)
			});


	}

	void CreateChunk(const glm::ivec2& chunkId)
	{
		if (chunks.find(chunkId) != chunks.end()) return;

		chunks.insert(std::make_pair(chunkId, std::make_shared<Chunk>(chunkId)));
		auto c = chunks.find(chunkId);
		std::map<glm::ivec2, std::shared_ptr<Chunk>>::iterator other;
		for (glm::ivec2 offset{ -1,-1 }; offset.y <= 1; offset.y++)
		{
			for (offset.x = -1; offset.x <= 1; offset.x++)
			{
				if ((other = chunks.find(chunkId + offset)) != chunks.end())
				{
					c->second->neighbours[offset.x + 1][offset.y + 1] = other->second;
					other->second->neighbours[-offset.x + 1][-offset.y + 1] = c->second;
				}
			}
		}
	}

	bool GetPlane(const glm::ivec2& position)
	{
		const glm::ivec2 id = position / static_cast<int>(Chunk::size);
		const auto c = chunks.find(id);
		if (c == chunks.end()) return true;
		return c->second->GetPlane(position);
	}

	void Render(const FrameInfo& info)
	{
		chunkShader->Use();

		chunkShader->SetUniform(0, info.projection);
		chunkShader->SetUniform(1, info.view);
		chunkShader->SetUniform(3, glm::vec3(1, 1, 1));

		for (const auto& c : chunks)
		{
			chunkShader->SetUniform(2, c.second->GetModelMatrix());
			c.second->Render();
		}

		projShader->Use();

		projShader->SetUniform(0, info.projection);
		projShader->SetUniform(1, info.view);

		for (const auto& c : chunks)
		{
			projShader->SetUniform(2, c.second->GetModelMatrix());
			c.second->RenderProjection();
		}
	}

	bool HasChunkAt(const glm::ivec2& id)
	{
		return chunks.find(id) != chunks.end();
	}

	std::shared_ptr<Chunk> GetChunkAt(const glm::ivec2& id)
	{
		auto chunk = chunks.find(id);
		return chunk != chunks.end() ? chunk->second : nullptr;
	}

	void Rebuild()
	{
		for (auto& c : chunks)
		{
			c.second->StartUpdate();
		}
		for (auto& c : chunks)
		{
			c.second->FinishUpdate();
		}
	}

	void Merge(const glm::vec3& position,const float& angle, short* depth,const glm::ivec2& resolution)
	{
		// Create neighbouring chunks
		const glm::ivec2 centerChunk = { glm::floor(position.x / Chunk::size) ,glm::floor(position.y / Chunk::size) };
		for (glm::ivec2 offset{ -2 ,-2}; offset.y <= 2; offset.y++)
		{
			for (offset.x = -2; offset.x <= 2; offset.x++)
			{
				if (!HasChunkAt(centerChunk + offset))
				{
					CreateChunk(centerChunk + offset);
				}
			}
		}

		try
		{
			// Run projection kernel
			depthMapBuf->Upload(queue, depth, resolution.x * resolution.y * sizeof(uint16_t));
			cl_float2 res;
			res.x = resolution.x;
			res.y = resolution.y;
			depthMapResBuf->Upload(queue, &res, sizeof(res));
			angleBuf->Upload(queue, &angle, sizeof(angle));
			fieldBuf->Upload(queue, emptyField.data(), emptyField.size() * sizeof(cl_int));

			projectionKernel.Execute(queue, resolution.x * resolution.y, 100);

			//Run merging kernel
			int32_t i = 4;
			for (int32_t y = -2; y <= 2; y++)
			{
				for (int32_t x = -2; x <= 2; x++)
				{
					mergerKernel.SetArg(i++, GetChunkAt(centerChunk + glm::ivec2(x, y))->GetBuffer());
				}
			}
			int32_t moddedChunks = 0;
			moddedChunksBuf->Upload(queue, &moddedChunks, sizeof(moddedChunks));

			cl_float3 pos;
			pos.x = position.x;
			pos.y = position.y;
			pos.z = position.z;
			positionBuf->Upload(queue, &pos, sizeof(pos));
			mergerKernel.Execute(queue, kernelInfo.fieldSize.x * kernelInfo.fieldSize.y * kernelInfo.fieldSize.z, 1);
			moddedChunksBuf->Download(queue, &moddedChunks, sizeof(moddedChunks));
			moddedChunks = 0xFFFFFFFF;
			// Update neighbouring chunks

			//auto start = std::chrono::high_resolution_clock::now();
			i = 0;
			for (int32_t y = -2; y <= 2; y++)
			{
				for (int32_t x = -2; x <= 2; x++)
				{
					if (!(moddedChunks >> i++)) continue;
					auto chunk = GetChunkAt(centerChunk+glm::ivec2{ x,y });
					if (chunk!=nullptr)
					{
						chunk->StartUpdate();
					}
				}
			}

			i = 0;
			for (int32_t y = -2; y <= 2; y++)
			{
				for (int32_t x = -2; x <= 2; x++)
				{
					auto chunk = GetChunkAt(centerChunk + glm::ivec2{ x,y });
					if (chunk != nullptr && moddedChunks >> i++)
					{
						chunk->FinishUpdate();
					}
				}
			}

			//std::cout << "Update took:" << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count() << std::endl;
		}
		catch (std::exception & e)
		{
			std::cerr << "Failed to merge depth map to map: " << e.what() << std::endl;
		}
	}
private:
	void InitKernel()
	{
		try
		{
			// Kernel info
			kernelInfoBuf = std::make_shared<ocl::Buffer>(Globals::oclContext, sizeof(kernelInfo), CL_MEM_READ_ONLY);
			kernelInfoBuf->Upload(queue, &kernelInfo, sizeof(kernelInfo));
			
			// Depth map
			depthMapBuf = std::make_shared<ocl::Buffer>(Globals::oclContext, Robot::kinectResolution.x * Robot::kinectResolution.y * sizeof(uint16_t), CL_MEM_READ_ONLY);
			depthMapResBuf = std::make_shared<ocl::Buffer>(Globals::oclContext, sizeof(cl_float2), CL_MEM_READ_ONLY);

			// Field
			fieldBuf = std::make_shared<ocl::Buffer>(Globals::oclContext, kernelInfo.fieldSize.x * kernelInfo.fieldSize.y * kernelInfo.fieldSize.z * sizeof(cl_int), CL_MEM_READ_WRITE);

			// Robot params
			angleBuf = std::make_shared<ocl::Buffer>(Globals::oclContext, sizeof(float), CL_MEM_READ_ONLY);
			positionBuf = std::make_shared<ocl::Buffer>(Globals::oclContext, sizeof(cl_float3), CL_MEM_READ_ONLY);

			// Projection kernel
			projectionKernel.Create(Globals::oclContext, "depthmapprojection", "../../Resources/Kernels/DepthMapProjection.cl", ocl::Kernel::FromFile);
			projectionKernel.SetArg(0, *kernelInfoBuf);
			projectionKernel.SetArg(1, *angleBuf);
			projectionKernel.SetArg(2, *depthMapBuf);
			projectionKernel.SetArg(3, *depthMapResBuf);
			projectionKernel.SetArg(4, *fieldBuf);

			// Merger parameters
			moddedChunksBuf = std::make_shared<ocl::Buffer>(Globals::oclContext, sizeof(int32_t), CL_MEM_READ_WRITE);

			// Merger kernel
			mergerKernel.Create(Globals::oclContext, "depthfieldmerger", "../../Resources/Kernels/DepthFieldMerger.cl", ocl::Kernel::FromFile);
			mergerKernel.SetArg(0, *kernelInfoBuf);
			mergerKernel.SetArg(1, *positionBuf);
			mergerKernel.SetArg(2, *fieldBuf);
			mergerKernel.SetArg(3, *moddedChunksBuf);

			Chunk::Init(kernelInfoBuf);
		}
		catch (std::exception & e)
		{
			std::cerr <<"OpenCL kernel creation error: " << e.what() << std::endl;
		}
	}
};