#pragma once

#include "VertexArray.hpp"
#include "Shader.hpp"
#include "Transformable.hpp"
#include <optional>
#include "Entity.hpp"

class WorldCursor : public Transformable, public Entity
{
	static constexpr float topHeight = 100.f;
	static constexpr float topRadius = 25.f;
	static constexpr float bottomHeight = 100.f;
	static constexpr float bottomRadius = 50.f;
	std::shared_ptr<Shader> shader;
	VertexArray va;
	int32_t count = 0;
	bool visible = true;
public:
	virtual void LoadResources()
	{
		shader = Shader::LoadFromMemory({
			"WorldCursor",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec3 aPos; \n
layout(location = 1) in float aShading; \n
layout(location = 2) uniform mat4 projection; \n
layout(location = 3) uniform mat4 view; \n
layout(location = 4) uniform mat4 model; \n
layout(location = 5) out float shading; \n
void main() {
\n
gl_Position = projection * view * model * vec4(aPos, 1); \n
shading = aShading; \n
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor; \n
layout(location = 5) in float shading; \n
void main()\n
{ \n
	FragColor = vec4(vec3(1,1,0)*shading,1); \n
}\n
#endif
			)
			});
		std::vector<glm::vec4> vtx;
		static constexpr float step = glm::two_pi<float>() / 32.f;
		for (float i = 0; i < glm::two_pi<float>(); i+=step)
		{
			const float isin = std::sin(i);
			const float icos = std::cos(i);
			const float isin2 = std::sin(i+step);
			const float icos2 = std::cos(i+step);
			float shading = i/glm::pi<float>();
			if (shading > 1) shading = 1 - (shading - 1);
			shading = 0.3 + 0.7 * shading;

			// Rod top
			vtx.push_back({icos*topRadius,isin*topRadius,bottomHeight+topHeight,shading});
			vtx.push_back({icos2*topRadius,isin2*topRadius,bottomHeight+topHeight,shading });
			vtx.push_back({0,0,bottomHeight+topHeight,shading });

			// Rod side
			vtx.push_back({ icos * topRadius,isin * topRadius,bottomHeight + topHeight,shading });
			vtx.push_back({ icos * topRadius,isin * topRadius,bottomHeight ,shading });
			vtx.push_back({ icos2 * topRadius,isin2 * topRadius,bottomHeight + topHeight,shading });

			vtx.push_back({ icos * topRadius,isin * topRadius,bottomHeight ,shading });
			vtx.push_back({ icos2 * topRadius,isin2 * topRadius,bottomHeight ,shading });
			vtx.push_back({ icos2 * topRadius,isin2 * topRadius,bottomHeight + topHeight,shading });

			// Cone top
			vtx.push_back({ icos * topRadius,isin * topRadius,bottomHeight ,shading });
			vtx.push_back({ icos * bottomRadius,isin * bottomRadius,bottomHeight ,shading });
			vtx.push_back({ icos2 * topRadius,isin2 * topRadius,bottomHeight ,shading });
			
			vtx.push_back({ icos2 * topRadius,isin2 * topRadius,bottomHeight,shading });
			vtx.push_back({ icos * bottomRadius,isin * bottomRadius,bottomHeight,shading });
			vtx.push_back({ icos2 * bottomRadius,isin2 * bottomRadius,bottomHeight ,shading });

			// Tip
			vtx.push_back({ icos * bottomRadius,isin * bottomRadius,bottomHeight,shading });
			vtx.push_back({ 0,0,0 ,shading });
			vtx.push_back({ icos2 * bottomRadius,isin2 * bottomRadius,bottomHeight ,shading });
		}
		count = vtx.size();
		va.FillVertices(vtx);
		va.SetAttribPointers({ {3,GL_FLOAT},{1,GL_FLOAT} });
	}

	virtual void Render(const FrameInfo& info)
	{
		if (!visible) return;
		shader->Use();
		shader->SetUniform(2, info.projection);
		shader->SetUniform(3, info.view);
		shader->SetUniform(4, GetTransform());

		va.DrawArrays(GL_TRIANGLES, 0, count);
	}

	void Update()
	{

	}

	void SetVisibility(const bool& state)
	{
		visible = state;
	}

	const bool IsVisible() const
	{
		return visible;
	}

	static std::optional<glm::vec3> IntersectRayWithPlane(glm::vec3 n, float d, glm::vec3 p, glm::vec3 v)
	{
		float denom = glm::dot(n, v);

		if (std::abs(denom) <= 1e-4f)
			return std::nullopt;

		if (-denom <= 1e-4f)
			return std::nullopt;

		float t = -(glm::dot(n, p) + d) / glm::dot(n, v);

		if (t <= 1e-4)
			return std::nullopt;

		return p + t * v;
	}
};