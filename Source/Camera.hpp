#include <glm/glm.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>

struct FrameInfo
{
	glm::mat4 view; // Current view matrix
	glm::mat4 projection; // Current view matrix
	glm::vec3 camera; // Camera position
	float elapsed = 0.0;  // Elapsed time in seconds
};


class Camera
{
protected:
	glm::vec3 position;
	glm::quat orientation;
	glm::vec2 resolution;

public:
	Camera(const glm::vec2 resolution) : position(-1000, 2000, 2000),
		orientation(glm::vec3(glm::half_pi<float>() * -0.5, 0, glm::half_pi<float>())),
		resolution(resolution)
	{
	}

	inline const void SetPositon(const glm::vec3 position)
	{
		this->position = position;
	}

	inline const glm::vec3& GetPosition() const
	{
		return position;
	}

	const void SetOrientation(const glm::quat orientation)
	{
		this->orientation = orientation;
	}

	const glm::quat& GetOrientation() const
	{
		return orientation;
	}

	inline const void Turn(glm::vec3 angle)
	{
		orientation = glm::rotate(glm::normalize(orientation), angle.x, glm::vec3(0, 0, 1));
		orientation = glm::rotate(glm::normalize(orientation), angle.y, glm::vec3(0, 1, 0)* orientation);
		/*
		orientation = glm::rotate(glm::normalize(orientation), angle.x, glm::vec3(0, 0, 1) * orientation);
		orientation = glm::rotate(glm::normalize(orientation), angle.y, glm::vec3(0, 1, 0) * orientation);
		orientation = glm::rotate(glm::normalize(orientation), angle.z, glm::vec3(1, 0, 0) * orientation);
		*/
	}

	inline const void Move(glm::vec3 direction)
	{
		position += (glm::vec3(1, 0, 0) * orientation) * direction.x;
		position += (glm::vec3(0, 1, 0) * orientation) * direction.y;
		position += (glm::vec3(0, 0, 1) * orientation) * direction.z;
		if (position.z < 0) position.z = 0;
	}

	inline const glm::highp_mat4 CalculateView()
	{
		const glm::vec3 eye = position + (glm::vec3(0, 1, 0) * orientation);
		return glm::lookAt(eye, eye + (glm::vec3(1, 0, 0) * orientation), (glm::vec3(0, 0, 1) * orientation));
	}

	inline const glm::highp_mat4 CalcProjection()
	{
		return glm::perspective(glm::radians(90.f), resolution.x / resolution.y, 0.1f, 1000000.f);
	}
	
	inline glm::vec3 CalcWorldVector(const glm::vec2& screenPos,const glm::vec2& screenResolution,const glm::mat4& view, const glm::mat4& projection)
	{
		return glm::normalize(glm::vec3(
			glm::inverse(projection * view)	* glm::vec4(glm::vec2(screenPos / screenResolution * 2.f -1.f), 1.0f, 1.0f)
		));
	}
};
