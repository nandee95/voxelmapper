#pragma once
#include "OpenCl.hpp"

static class Globals
{
public:
	static ocl::Context oclContext;

	static void InitOpenCL()
	{
		// Get OpenCL devices
		auto devices = ocl::GetDevices();
		if (devices.size() == 0)
		{
			std::cerr << "No OpenCL device found!" << std::endl;
			exit(EXIT_FAILURE);
		}

		// Select the most capable device
		ocl::OCLDevice* selection = &devices[0];
		for (auto& d : devices)
		{
			if ((d.isGpu && selection->isGpu) && d.memorySize > selection->memorySize)
			{
				selection = &d;
			}
		}
		std::cout << "OpenCL Device selected: " << selection->name << std::endl;
		oclContext.Create(*selection);
	}
};
ocl::Context Globals::oclContext;