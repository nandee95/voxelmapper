#pragma once

#include<vector>
#include<memory>

class TempEntity
{

protected:

	static std::vector<std::shared_ptr<TempEntity>> entities;
	virtual bool Render(const FrameInfo& info) = 0;

public:

	static void Add(std::shared_ptr<TempEntity> entity)
	{
		entities.push_back(entity);
	}

	static void RenderAll(const FrameInfo& info)
	{
		for (auto it = entities.begin(); it != entities.end();)
		{
			if ((*it)->Render(info))
				it++;
			else it = entities.erase(it);
		}
	}
};

std::vector<std::shared_ptr<TempEntity>> TempEntity::entities;