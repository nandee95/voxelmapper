#pragma once

#include <stdint.h>
#include "VertexArray.hpp"
#include "Shader.hpp"
#include "Texture.hpp"
#include "Entity.hpp"
#include <string>

class Preview : public Entity
{
	static constexpr float animTime = 0.2;
	static constexpr float idleScale = 0.2;
	static constexpr float focusScale = 0.5;
	static constexpr glm::vec2 idlePos{ 0.75,-0.75 };
	static constexpr glm::vec2 focusPos{ 0,0 };
	VertexArray va;
	std::shared_ptr<Shader> shader;
	bool enabled = false;
	size_t count = 0;
	float transition = 0;
	std::chrono::high_resolution_clock::time_point last;
	Text info;
	glm::vec2 position;
	bool inBounds = false;
public:
	Texture texture;
	Preview()
	{
		info.SetText("?? mm");
	}

	void MovePointer(const glm::ivec2& pos,const glm::ivec2 resolution)
	{
		info.SetPosition(pos+glm::ivec2(25,0));
		position = glm::vec2(pos) / glm::vec2(resolution);
		const glm::vec2 checkPos = position * 2.f - 1.f;
		inBounds = abs(checkPos.x) < focusScale && abs(checkPos.y) < focusScale;
	}

	virtual void LoadResources()
	{
		shader = shader->LoadFromMemory({
			"Preview",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec2 aPos; \n
layout(location = 1) in vec2 uv; \n
layout(location = 2) out vec2 texUv; \n
layout(location = 4) uniform float scale = 1.0; \n
layout(location = 5) uniform vec2 position = vec2(0,0); \n
void main() {
	\n
gl_Position = vec4(position + aPos * scale, 0, 1); \n
texUv = uv; \n
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor; \n
layout(location = 2) in vec2 texUv; \n
layout(location = 3) uniform sampler2D tex; \n
vec3 hsv2rgb(vec3 c)
{
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
			return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}
void main()\n
{ \n
	float value = texture2D(tex, texUv).r;
	FragColor = vec4(value == 0 ? vec3(0.2,0,0) : hsv2rgb(vec3((value*1.2)*3.1415*2,1,1)),1); \n
}\n
#endif
			)
			});
		shader->SetUniform(3, 0);

		struct Vertex
		{
			glm::vec2 pos;
			glm::vec2 uv;
		};

		std::vector<Vertex> vtx;
		vtx.push_back({ { 1,-1 },{1,1} });
		vtx.push_back({ { 1,1 } ,{1,0} });
		vtx.push_back({ { -1,-1 },{0,1} });
		vtx.push_back({ { -1,1 },{0,0} });

		va.FillVertices(vtx);
		va.SetAttribPointers({ {2,GL_FLOAT} ,{2,GL_FLOAT} });

		count = vtx.size();
	}

	virtual void Render(const FrameInfo& info)
	{
		transition = std::fmax(std::fmin(transition + (enabled ? 1.f : -1.f) * static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - last).count()) / 1000000.f / animTime, 1.f), 0.f);

		last = std::chrono::high_resolution_clock::now();

		glDisable(GL_DEPTH_TEST);
		shader->Use();
		texture.Bind(0);
		shader->SetUniform(4, idleScale  + transition*(focusScale - idleScale));
		shader->SetUniform(5, idlePos  + transition*(focusPos - idlePos));
		va.DrawArrays(GL_TRIANGLE_STRIP, 0, count);
		if (transition == 1 && inBounds)
		{
			this->info.Render();
		}
		glEnable(GL_DEPTH_TEST);
	}

	void Toggle()
	{
		enabled = !enabled;
	}

	void Set(const uint16_t* data, const glm::ivec2& resolution)
	{
		if(inBounds)
			info.SetText(std::to_string(data[(int)(position.y * resolution.y) * resolution.x + (int)(position.x * resolution.x)])+" mm");
		texture.Create(resolution.x, resolution.y, data, GL_RED, GL_UNSIGNED_SHORT);
	}
};