#pragma once

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

class Colorable
{
protected:
	glm::vec4 color = {1,1,1,1};
public:
	void SetColor(const glm::vec3& color)
	{
		this->color = glm::vec4(color, 1);
	}

	void SetColor(const glm::vec4& color)
	{
		this->color = color;
	}

	const glm::vec4& GetColor() const
	{
		return color;
	}


};