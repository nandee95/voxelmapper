#pragma once

#include "VertexArray.hpp"
#include "Shader.hpp"
#include "Entity.hpp"

class CoordinateSystem : public Entity
{
	VertexArray va;
	std::shared_ptr<Shader> shader;
public:

	virtual void LoadResources()
	{
		shader = Shader::LoadFromMemory({
			"CoordinateSystem",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec3 aPos; \n
layout(location = 1) in vec3 aColor; \n
layout(location = 2) uniform mat4 projection; \n
layout(location = 3) uniform mat4 view; \n
layout(location = 4) out vec3 color; \n
void main() {
\n
gl_Position = projection * view * vec4(aPos+vec3(0,0,10), 1); \n
color = aColor;
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor; \n
layout(location = 4) in vec3 color;
void main()\n
{ \n
	FragColor = vec4(color,1); \n
}\n
#endif
			)
		});
		struct Vertex
		{
			glm::vec3 pos;
			glm::vec3 color;
		};
		std::vector<Vertex> vtx;
		//X
		vtx.push_back({ {1000,0,0},{1,0,0} });
		vtx.push_back({ {0,0,0},{1,0,0} });
		//Y
		vtx.push_back({ {0,1000,0},{0,1,0} });
		vtx.push_back({ {0,0,0},{0,1,0} });
		//Z
		vtx.push_back({ {0,0,1000},{0,0,1} });
		vtx.push_back({ {0,0,0},{0,0,1} });


		va.FillVertices(vtx);
		va.SetAttribPointers({ {3,GL_FLOAT},{3,GL_FLOAT} });
	}


	virtual void Render(const FrameInfo& info)
	{
		shader->Use();
		shader->SetUniform(2, info.projection);
		shader->SetUniform(3, info.view);
		va.DrawArrays(GL_LINES, 0, 6);
	}

};