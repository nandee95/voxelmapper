#pragma once
struct FrameInfo;

class Entity
{
public:
	virtual void LoadResources()=0;
	virtual void Render(const FrameInfo& info)=0;
};