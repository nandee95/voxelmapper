#pragma once
#include <stdint.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

enum class PacketType : uint8_t
{
	Packet_Undefined,
	Packet_FullUpdate,
	Packet_Control,
	Packet_Disconnect
};

struct PacketFullUpdate
{
	// Sensor data
	glm::vec3 position;
	float angle;
	// Kinect data
	glm::ivec2 resolution;
	int32_t totalSize;
	int32_t compressedSize;
	// followed by compressedSize bytes of zipped data
};

struct PacketControl
{
	float leftright;
	float frontback;
};