#pragma once

#include "Entity.hpp"
#include "VertexArray.hpp"
#include "Shader.hpp"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

class Path : public Entity
{
	static constexpr float height = 150.f;
	static constexpr float distanceLimit = 5000.f;
	DynamicVertexArray drawPath;
	static std::shared_ptr<Shader> shader;
	size_t vertices = 0;

	struct Weight {
		int g;
		int h;
		int f;
	};

	struct Node
	{
		glm::ivec2 parentPos;
		bool checked = false;
		Weight weight;
	};

	const int D1 = 10; //vertical and horizontal movement cost
	const int D2 = (int)(sqrt(2) * 10); //diagonal movement cost

public:

	virtual void LoadResources() override
	{
		shader = Shader::LoadFromMemory({
			"Path",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec3 aPos; \n
layout(location = 2) uniform mat4 projection; \n
layout(location = 3) uniform mat4 view; \n
void main() {
\n
gl_Position = projection * view * vec4(aPos, 1); \n
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor; \n
void main()\n
{ \n
	FragColor = vec4(vec3(0.05,0.6,0.1),1); \n
}\n
#endif
			)
			});
	}

	int GetWeights(const glm::ivec2 from, const glm::ivec2 to)
	{
		int weight;
		sf::Vector2i d;

		d.x = abs(from.x - to.x);
		d.y = abs(from.y - to.y);

		if (d.x > d.y)
			weight = (D1 * (d.x - d.y) + D2 * d.y);
		else
			weight = (D1 * (d.y - d.x) + D2 * d.x);

		return weight;
	}

	bool Plan(std::shared_ptr<Map> map, const glm::vec3& _startPos, const glm::vec3& _goalPos)
	{

		if (glm::distance(_startPos, _goalPos) > distanceLimit)
		{
			vertices = 0;
			return false;
		}

		std::map<glm::ivec2, Node> openList;
		std::map<glm::ivec2, Node> closedList;
		//map->GetPlane({ 1,2 });
		// A* implementation awaits
		bool foundDestination = false;
		
		const glm::ivec2 startPos = { floor(_startPos.x / 50.f), floor(_startPos.y / 50.f) };
		const glm::ivec2 goalPos = { floor(_goalPos.x / 50.f), floor(_goalPos.y / 50.f) };

		if (startPos == goalPos)
		{
			vertices = 0;
			return false;
		}

		//Adding bonus for F that is closer to the goal
		float bonusAmount = 1.0f;
		int distanceToGoal = GetWeights(startPos, goalPos);

		//Prepare start node
		Node startNode;
		startNode.weight.g = 0;
		startNode.weight.h = GetWeights(startPos, goalPos);
		startNode.weight.f = startNode.weight.g + startNode.weight.h;
		closedList.insert(std::make_pair(startPos, startNode));

		//0-free 1-blocked 2-start 3-goal 4-openList 5-closedList
		int entries = 0;

		while (!foundDestination)
		{
			for (auto& c : closedList)
			{
				if (c.second.checked == false)
				{
					std::vector<Node> neighbours;
					//Lookup the neighbours
					for (short x = -1; x <= 1; x++) //X coordinate
					{
						for (short y = -1; y <= 1; y++) //Y coordinate
						{
							const glm::ivec2 currentPos(c.first.x + x, c.first.y + y);
							if (closedList.find(currentPos) == closedList.end())
							{
								entries++;
								if (map->GetPlane({ currentPos.x, currentPos.y }) || currentPos == goalPos)
								{
									Node currentNode;
									currentNode.parentPos = c.first;
									currentNode.weight.g = GetWeights(c.first, currentPos) + c.second.weight.g; // + that->closedList[i].weight.x //maybe shorter but more time
									currentNode.weight.h = GetWeights(currentPos, goalPos);

									if (currentNode.weight.h <= distanceToGoal)
									{
										currentNode.weight.f = currentNode.weight.g + currentNode.weight.h -
											(int)((distanceToGoal - currentNode.weight.h) * bonusAmount);
									}

									if (currentNode.weight.h > distanceToGoal)
									{
										currentNode.weight.f = currentNode.weight.g + currentNode.weight.h +
											(int)((currentNode.weight.h - distanceToGoal) * bonusAmount);
									}

									openList.insert(std::make_pair(currentPos, currentNode));
								}
							}
						}
					}
					c.second.checked = true;
				}
			}

			//Check if openList empty and return error (no path found)
			if (openList.size() == 0)
			{
				return false;
			}

			//Select node to lock down
			int smallestValue = 0;
			glm::ivec2 smallestIndex = { 0,0 };

			auto first = openList.begin();
			smallestValue = first->second.weight.f;
			smallestIndex = first->first;
			for (const auto& g : openList)
			{
				if (g.second.weight.f < smallestValue)
				{
					smallestValue = g.second.weight.f;
					smallestIndex = g.first;
				}
			}

			//Add selected node to the closed list
			closedList.insert(std::make_pair(smallestIndex, openList[smallestIndex]));

			if (smallestIndex == goalPos)
			{
				//Remove selected node from open list
				openList.erase(openList.find(smallestIndex));

				foundDestination = true;
				break;
			}

			//Remove selected node from open list
			openList.erase(openList.find(smallestIndex));

			//std::this_thread::sleep_for(std::chrono::milliseconds(200)); //sleep for testing
		}

		//Search best path in closed list
		//Write the goal position into the path
		auto e = closedList.find(goalPos);


		//Setup vector for final path
		std::vector<glm::vec3> foundPath;
		foundPath.push_back(glm::vec3(glm::vec2(e->first) *50.f + glm::vec2(25, 25), height));

		glm::ivec2 index = e->second.parentPos;
		while (true)
		{
			e = closedList.find(index);

			int weightF = e->second.weight.f;
			int weightG = e->second.weight.g;
			bool foundBetter = false;

			for (short x = -1; x <= 1; x++) {
				for (short y = -1; y <= 1; y++) {
					const auto f = closedList.find(glm::ivec2(e->first.x + x, e->first.y + y));
					if (f != closedList.end())
					{
						if (f->second.weight.f <= weightF && f->second.weight.g < weightG)
						{
							index = f->first;
							weightF = f->second.weight.f;
							weightG = f->second.weight.g;
							foundBetter = true;
						}
					}
				}
			}

			if (foundBetter == true)	e = closedList.find(index);

			if (e->first == startPos)	break;
			foundPath.push_back(glm::vec3(glm::vec2(e->first)*50.f + glm::vec2(25, 25), height));


			index = e->second.parentPos;
		}
		
		vertices = foundPath.size();
		drawPath.FillVertices(foundPath);
		drawPath.SetAttribPointers({ {3,GL_FLOAT} });

		//if (glm::distance(_startPos, _goalPos) < 3000.f)
		//	return false;

		//Example code :3
		/*std::vector<glm::vec3> v;
		v.push_back(startPos);
		v.push_back(endPos);
		vertices = v.size();
		drawPath.FillVertices(v.data(), v.size() * sizeof(glm::vec3));
		drawPath.SetAttribPointers({ {3,GL_FLOAT} });
		return glm::distance(startPos,endPos) < 3000.f;*/
		if (!foundDestination)
		{
			vertices = 0;
		}
		return foundDestination;
	}

	virtual void Render(const FrameInfo& info) override
	{
		shader->Use();
		shader->SetUniform(2, info.projection);
		shader->SetUniform(3, info.view);
		glLineWidth(10.f);
		drawPath.DrawArrays(GL_LINE_STRIP, 0, vertices);
		glLineWidth(1.f);
	}
};

std::shared_ptr<Shader> Path::shader;