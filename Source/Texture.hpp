#pragma once
#include <string>
#include <GL/glew.h>

class Texture
{
protected:
	GLuint id;
public:
	Texture()
	{
		glGenTextures(1, &id);
	}

	~Texture()
	{
		glDeleteTextures(1, &id);
	}

	void Create(const GLuint width, const GLuint height, const void* pixels, const GLenum colors = GL_RGBA,const GLenum type=GL_FLOAT) const noexcept
	{
		Bind(0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, colors, width, height, 0, colors, type, pixels);
	}

	void SetFiltering(const GLenum value) const noexcept
	{
		glBindTexture(GL_TEXTURE_2D, id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, value);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, value);
	}

	void SetWrapping(const GLenum value) const noexcept
	{
		glBindTexture(GL_TEXTURE_2D, id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, value);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, value);
	}

	void GenerateMipmap() const noexcept
	{
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	inline void Bind(const GLuint index = 0) const noexcept
	{
		glActiveTexture(GL_TEXTURE0 + index);
		glBindTexture(GL_TEXTURE_2D, id);
	}

	const GLuint& GetId() const noexcept
	{
		return id;
	}

};
