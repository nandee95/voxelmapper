#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>

class Transformable
{
protected:
	glm::vec3 position {0,0,0};
	glm::quat orientation { 0,0,0,0 };
	float scale = 1.f;
public:
	void SetPosition(const glm::vec3 position)
	{
		this->position = position;
	}

	const glm::vec3& GetPosition() const
	{
		return position;
	}

	void SetScale(const float scale)
	{
		this->scale = scale;
	}

	const float& GetScale() const
	{
		return scale;
	}

	const glm::quat& GetOrientation() const
	{
		return orientation;
	}

	void SetOrientation(const glm::quat orientation)
	{
		this->orientation = orientation;
	}

	void SetOrientation(const glm::vec3 euler)
	{
		orientation = glm::quat(euler);
	}

	glm::mat4 GetTransform()
	{
		glm::mat4 trans = glm::mat4(1.0f);
		trans = glm::translate(trans, position);
		trans = glm::scale(trans, glm::vec3(scale, scale, scale));
		trans *= glm::toMat4(orientation);
		return trans;
	}
};