#pragma once

// Standard
#include <stdint.h>
#include <vector>
#include <thread>
#include <functional>
#include <chrono>
// Graphics
#include "VertexArray.hpp"
#include "Shader.hpp"
#include "Transformable.hpp"
#include <glm/gtx/normal.hpp>
#include "Entity.hpp"
// Network
#include <SFML/Network.hpp>
#include "Compressor.hpp"
#include "Protocol.hpp"

class Robot : public Transformable, public Entity
{
public:
	static constexpr float radius = 250;
	static constexpr float height = 70;
	static constexpr float kinectRange = 3500.f;
	static constexpr glm::vec3 kinectOffset = glm::vec3(11.7, 150, 117.4); // glm::vec3(11.7, -25, 117.4)
	static constexpr glm::vec2 kinectFOV = { glm::radians(62.f) , glm::radians(48.6) };
	static constexpr glm::vec2 kinectResolution = { 640,480 };
	static constexpr float kinectangle = glm::radians(10.f);
	static constexpr glm::vec3 bodyDef[] = {
		{42.5,-155,36},
		{87.853,-128.815,40.28},
		{145,-45,54},
		{145,45,54},
	};
	static constexpr float bodyHeight = 10.f;

	bool connected = false;

private:
	// Networking
	sf::TcpSocket socket;
	uint16_t port;
	bool running = true;
	std::thread thread;
	std::function<void(uint16_t * frame, int32_t frameSize, glm::vec3 position, float angle)> onFrame;

	// Graphics
	struct Vertex
	{
		glm::vec3 aPos;
		glm::vec3 aNorm;
	};


	VertexArray va, va2;
	std::shared_ptr<Shader> shader;
	size_t count = 0, count2 = 0;
	float angle = glm::half_pi<float>();
	int32_t fps=0.f;
	std::thread fpsThread;
	int32_t frames;
public:
	Robot(const uint16_t& port) : port(port), thread(&NetworkThread, this), fpsThread(&FpsThread,this)
	{
	}

	~Robot()
	{
		socket.disconnect();
		running = false;
		if (thread.joinable()) thread.join();
		if (fpsThread.joinable()) fpsThread.join();
	}

	void Send(void* data, const size_t& size)
	{
		socket.send(data, size);
	}

	static void FpsThread(Robot* that)
	{
		while (that->running)
		{
			that->fps = that->frames;
			that->frames = 0;
			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
	}


	virtual void LoadResources()
	{
		shader = Shader::LoadFromMemory({
			"Robot",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec3 aPos; \n
layout(location = 1) in vec3 aNorm; \n
layout(location = 2) uniform mat4 projection; \n
layout(location = 3) uniform mat4 view; \n
layout(location = 4) uniform mat4 model; \n
layout(location = 6) out vec3 norm; \n
layout(location = 7) out vec3 frag; \n
void main() {
\n
gl_Position = projection * view * model * vec4(aPos, 1); \n
norm = aNorm; \n
frag = vec3(model * vec4(aPos, 1.0)); \n
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor; \n
layout(location = 5) uniform vec4 color = vec4(1,0,0,1);
layout(location = 6) in vec3 norm; \n
layout(location = 7) in vec3 frag; \n
void main()\n
{ \n
float shading = max(dot(norm, vec3(1, 1, 1)), 0.0);
shading = 0.3 + 0.7 * shading; \n
	FragColor = vec4(color.rgb*shading,1); \n
}\n
#endif
			)
			});

		std::vector<Vertex> vtx;

		static constexpr glm::vec3 mirrorX = { -1,1,1 };
		static constexpr glm::vec3 supressZ = { 1,1,0 };
		for (int32_t i = 0; i < 3; i++)
		{
			const int32_t j = i + 1;
			
			// Bottom plate
			PushVertex(
				vtx,
				bodyDef[i] * supressZ,
				bodyDef[i] * supressZ * mirrorX,
				bodyDef[j] * supressZ * mirrorX
			);

			PushVertex(
				vtx,
				bodyDef[i] * supressZ,
				bodyDef[j] * supressZ * mirrorX,
				bodyDef[j] * supressZ);

			// Top plate
			PushVertex(
				vtx,
				bodyDef[i],
				bodyDef[j] * mirrorX,
				bodyDef[i] * mirrorX);

			PushVertex(
				vtx,
				bodyDef[i],
				bodyDef[j],
				bodyDef[j] * mirrorX);

			// Side plate
			PushVertex(
				vtx,
				bodyDef[i],
				bodyDef[i] * supressZ,
				bodyDef[j]);


			PushVertex(
				vtx,
				bodyDef[i] * supressZ,
				bodyDef[j] * supressZ,
				bodyDef[j]);

			// Side plate (mirrored)
			PushVertex(
				vtx,
				bodyDef[i] * mirrorX,
				bodyDef[j] * mirrorX,
				bodyDef[i] * supressZ * mirrorX);

			PushVertex(
				vtx,
				bodyDef[i] * supressZ * mirrorX,
				bodyDef[j] * mirrorX,
				bodyDef[j] * supressZ * mirrorX);
		}

		// Back plate
		PushVertex(
			vtx,
			bodyDef[3],
			bodyDef[3] * supressZ,
			bodyDef[3] * mirrorX);

		PushVertex(
			vtx,
			bodyDef[3] * mirrorX,
			bodyDef[3] * supressZ,
			bodyDef[3] * mirrorX * supressZ);

		// Front plate
		PushVertex(
			vtx,
			bodyDef[0],
			bodyDef[0] * mirrorX,
			bodyDef[0] * supressZ);


		PushVertex(
			vtx,
			bodyDef[0] * mirrorX,
			bodyDef[0] * mirrorX * supressZ,
			bodyDef[0] * supressZ);

		std::for_each(vtx.begin(), vtx.end(), [](Vertex& v) {
			v.aPos.z += bodyHeight;
			});

		va.FillVertices(vtx);
		va.SetAttribPointers({ {3,GL_FLOAT},{3,GL_FLOAT} });
		count = vtx.size();
		
		std::vector<glm::vec3> vtx2;

		//Boundaries
		vtx2.push_back(kinectOffset);
		vtx2.push_back(kinectOffset + glm::vec3(std::sinf(-kinectFOV.x / 2.f) * std::cosf(kinectFOV.y / 2.f + kinectangle), -std::cosf(-kinectFOV.x / 2.f) * std::cosf(kinectFOV.y / 2.f + kinectangle), std::sinf(kinectFOV.y / 2.f + kinectangle)) * kinectRange);

		vtx2.push_back(kinectOffset);
		vtx2.push_back(kinectOffset + glm::vec3(std::sinf(kinectFOV.x / 2.f) * std::cosf(kinectFOV.y / 2.f + kinectangle), -std::cosf(kinectFOV.x / 2.f) * std::cosf(kinectFOV.y / 2.f + kinectangle), std::sinf(kinectFOV.y / 2.f + kinectangle)) * kinectRange);

		vtx2.push_back(kinectOffset);
		vtx2.push_back(kinectOffset + glm::vec3(std::sinf(kinectFOV.x / 2.f) * std::cosf(-kinectFOV.y / 2.f + kinectangle), -std::cosf(kinectFOV.x / 2.f) * std::cosf(-kinectFOV.y / 2.f + kinectangle), std::sinf(-kinectFOV.y / 2.f + kinectangle)) * kinectRange);
		
		vtx2.push_back(kinectOffset);
		vtx2.push_back(kinectOffset + glm::vec3(std::sinf(-kinectFOV.x / 2.f) * std::cosf(-kinectFOV.y / 2.f + kinectangle), -std::cosf(-kinectFOV.x / 2.f) * std::cosf(-kinectFOV.y / 2.f + kinectangle), std::sinf(-kinectFOV.y / 2.f + kinectangle)) * kinectRange);


		va2.FillVertices(vtx2);
		va2.SetAttribPointers({ {3,GL_FLOAT} });
		count2 = vtx2.size();
	}
	virtual void Render(const FrameInfo& info)
	{
		shader->Use();
		shader->SetUniform(2, info.projection);
		shader->SetUniform(3, info.view);
		shader->SetUniform(4, GetTransform());
		shader->SetUniform(5, glm::vec4(1, 1, 0, 1));
		va.DrawArrays(GL_TRIANGLES, 0, count);
		shader->SetUniform(5, glm::vec4(1, 0, 0, 1));
		va2.DrawArrays(GL_LINES, 0, count2);
	}

	void ForwardBackward(const float& amt)
	{
		SetPosition(GetPosition() + (glm::vec3(sin(angle), -cos(angle), 0) * amt));
	}

	void Turn(const float& amt)
	{
		angle += amt;
		SetOrientation({ 0,0,angle });
	}


	void OnFrame(std::function<void(uint16_t * frame, int32_t frameSize, glm::vec3 position, float angle)> e)
	{
		onFrame = e;
	}

	const bool IsConnected() const
	{
		return connected;
	}

	const int32_t GetFps() const
	{
		return fps;
	}

protected:
	static void PushVertex(std::vector<Vertex>& vec, const glm::vec3& a, const glm::vec3& b, const glm::vec3& c)
	{
		glm::vec3 norm = glm::triangleNormal(a, b, c);
		vec.push_back(Vertex{ a,norm });
		vec.push_back(Vertex{ b, norm });
		vec.push_back(Vertex{ c, norm });
	}
private:
	static void NetworkThread(Robot* that)
	{
		connect:
		that->socket.setBlocking(true);
		std::cout << "Connecting to the robot on port "<< that->port << "..." << std::endl;
		while (that->socket.connect("192.168.0.101", that->port) != sf::Socket::Done)
		{
			std::cout << "Socket failed to connect! Retrying in 1 second..." << std::endl;
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
		std::cout << "Socket connected!" << std::endl;
		that->connected = true;
		PacketType type;
		size_t received;
		PacketFullUpdate fullupdate;
		sf::Socket::Status status;
		int32_t total = 0;
		std::vector<uint8_t> compressedFrame;
		while (that->running)
		{
			status = that->socket.receive(&type, sizeof(type), received);
			if (status == sf::Socket::Disconnected) goto disconnected;
			else if (status == sf::Socket::Done)
			{
				if (type == PacketType::Packet_FullUpdate)
				{
					status = that->socket.receive(&fullupdate, sizeof(fullupdate), received);
					if (status == sf::Socket::Disconnected) goto disconnected;
					else if (status == sf::Socket::Done)
					{
						total = 0;
						compressedFrame.resize(fullupdate.compressedSize);
						while (total < fullupdate.compressedSize && (status = that->socket.receive(compressedFrame.data() + total,compressedFrame.size() - total,received)) == sf::Socket::Done)
						{
							total += received;
						}

						if (total == fullupdate.compressedSize)
						{
							auto frame = Compressor::Decompress(compressedFrame.data(), fullupdate.compressedSize, fullupdate.totalSize);
							//std::cout << "Decompression took: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count() << "ms" << std::endl;
							if(that->onFrame)
								that->onFrame(reinterpret_cast<uint16_t*>(frame.data()), frame.size(), fullupdate.position, fullupdate.angle);
							that->frames++;
							that->angle = -glm::radians(fullupdate.angle);
							that->SetOrientation({ 0,0,that->angle });
							that->SetPosition(fullupdate.position);
						}
					}
				}
				else if (type == PacketType::Packet_Disconnect)
				{
					std::cout << "Robot sent disconnection request!" << std::endl;
					that->socket.disconnect();
					goto disconnected;
				}
			}
		}
	disconnected:
		std::cout << "Socket disconnected!" << std::endl;
		that->connected = false;
		if (that->running) goto connect;
	}
};