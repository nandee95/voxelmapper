#pragma once

#include <GL/glew.h>
#define GLT_IMPLEMENTATION
#include <gltext.h>
#include <string>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>


class Text
{
protected:
	GLTtext* text=nullptr;
	glm::vec4 color = {1,1,1,1};
	glm::vec2 position = { 0,0 };
	float scale = 1.0;
public:
	Text() : text(gltCreateText())
	{}

	~Text()
	{
		gltDestroyText(text);
	}

	static void Init(const glm::vec2& viewport)
	{
		gltInit();
		gltViewport(viewport.x, viewport.y);
	}

	void Render()
	{
		gltBeginDraw();

		gltColor(1.0f, 1.0f, 1.0f, 1.0f);
		gltDrawText2D(text, position.x, position.y, scale);

		gltEndDraw();
	}

	void SetText(const std::string& string)
	{
		gltSetText(text, string.c_str());
	}

	void SetColor(const glm::vec4& color)
	{
		this->color = color;
	}

	void SetSccale(float scale)
	{
		this->scale = scale;
	}

	void SetPosition(const glm::vec2& position)
	{
		this->position = position;
	}
};
