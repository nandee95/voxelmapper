#pragma once
#include "Window.hpp"
#include <iostream>
#include "Text.hpp"
#include "Camera.hpp"
#include "Grid.hpp"
#include "Robot.hpp"
#include "CoordinateSystem.hpp"
#include "Preview.hpp"
#include "Protocol.hpp"
#include "BeamRing.hpp"
#include "WorldCursor.hpp"
#include "Map.hpp"
#include "Hud.hpp"
#include "Path.hpp"
#include <chrono>
#include <mutex>

#include "EntityManager.hpp"

#include "Compressor.hpp"

#define NOMINMAX
#include <Windows.h>

class Application
{
	static constexpr uint16_t port = 3000;
	static constexpr float cameraSpeed = 1000.f;
	static constexpr float cameraSensitivity = 0.007;
	static constexpr bool manualControl = false;


	glm::vec2 resolution = { 1280,720 };
	FrameInfo info;

	int32_t fps, fpsCount;
	std::chrono::high_resolution_clock::time_point lastFpsMeasure;
public:

	Application()
	{
		
	}

	int32_t Run()
	{
	connect:

		// Init window
		Window window(resolution, "VoxelMapper", true);
		Camera camera(resolution);

		Globals::InitOpenCL();

		// Init opengl
		glViewport(0, 0, resolution.x, resolution.y);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glEnable(GL_BLEND);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glClearColor(0, 0, 0, 1);

		// Init resources
		BeamRing::LoadResources();
		Text::Init(resolution);

		EntityManager em;
		em.Add(std::make_shared<Grid>());
		em.Add(std::make_shared<CoordinateSystem>());
		auto robot = std::make_shared<Robot>(port);
		em.Add(robot);
		auto wc = std::make_shared<WorldCursor>();
		em.Add(wc);
		auto map = std::make_shared<Map>();
		em.Add(map);
		auto preview = std::make_shared<Preview>();
		em.Add(preview);
		auto path = std::make_shared<Path>();
		em.Add(path);

		Hud hud;
		hud.LoadResources(resolution);

		// Mouse move event
		bool drag = false;
		glm::vec2 lastPos = { 0,0 };
		window.RegisterEvent(WindowEventType::Event_MouseMove, [&](const WindowEventParams& params) {

			if (drag)
			{
				glm::vec2 diff = (params.position - lastPos) * cameraSensitivity;
				camera.Turn({
					diff.x,
					-diff.y,
					0
					});
				auto pos = params.position;

				if (pos.x > resolution.x) pos.x -= resolution.x;
				if (pos.x < 0) pos.x += resolution.x;
				if (pos.y > resolution.y) pos.y -= resolution.y;
				if (pos.y < 0) pos.y += resolution.y;
				window.SetCursorPos(pos);
			}
			preview->MovePointer(params.position,resolution);
			lastPos = params.position;

		});

		window.RegisterEvent(WindowEventType::Event_MouseClick, [&](const WindowEventParams& params) {
			if (params.key == GLFW_MOUSE_BUTTON_RIGHT)
			{
				drag = params.action == GLFW_PRESS;
				wc->SetVisibility(params.action != GLFW_PRESS);
			}

			if (params.key == GLFW_MOUSE_BUTTON_LEFT && params.action == GLFW_PRESS && !drag)
			{
				if (wc->IsVisible() && params.position.x > 0 && params.position.y > 0 && params.position.x < resolution.x && params.position.y < resolution.y)
				{
					const bool result = path->Plan(map, robot->GetPosition(), wc->GetPosition());
					auto ring = std::make_shared<BeamRing>();
					ring->SetPosition(wc->GetPosition());
					ring->SetColor(result ? glm::vec4{0, 1, 0, 1 } : glm::vec4{ 1, 0, 0, 1 });
					ring->SetRadius(1000.f);
					ring->SetDuration(250.f);
					TempEntity::Add(ring);
					std::cout << "Worldcursor clicked!" << std::endl;

				}
			}
		});

		// Key event
		window.RegisterEvent(WindowEventType::Event_Key, [&](const WindowEventParams& params) {
			if (params.key == GLFW_KEY_C && params.action == GLFW_PRESS)
			{
				preview->Toggle();
				std::cout << "Preview toggled!" << std::endl;
			}
			if (params.key == GLFW_KEY_F11 && params.action == GLFW_PRESS)
			{
				window.ToggleFullscreen();
				resolution = window.GetSize();
				glViewport(0, 0, resolution.x, resolution.y);
			}
			if (params.key == GLFW_KEY_E && params.action == GLFW_PRESS)
			{
				std::cout << "Rebuilding map..." << std::endl;
				auto start = std::chrono::high_resolution_clock::now();
				map->Rebuild();
				std::cout << "Map rebuilt! Took " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count() << " ms" << std::endl;
			}
			if (params.key == GLFW_KEY_ESCAPE && params.action == GLFW_PRESS)
			{
				window.Close();
				exit(EXIT_SUCCESS);
			}

			if (params.key == GLFW_KEY_LEFT || params.key == GLFW_KEY_RIGHT || params.key == GLFW_KEY_UP || params.key == GLFW_KEY_DOWN)
			{
				PacketControl packet;
				packet.leftright = window.IsKeyPressed(GLFW_KEY_LEFT) ? -1 : (window.IsKeyPressed(GLFW_KEY_RIGHT) ? 1 : 0);
				packet.frontback = window.IsKeyPressed(GLFW_KEY_DOWN) ? -1 : (window.IsKeyPressed(GLFW_KEY_UP) ? 1 : 0);
				PacketType type = PacketType::Packet_Control;
				robot->Send(&type, sizeof(type));
				robot->Send(&packet, sizeof(packet));
			}
		});


		// Frame info
		auto lastframe = std::chrono::high_resolution_clock::now();
		info.projection = camera.CalcProjection();


		bool newFrame = false;
		std::mutex kinectFrameMutex;
		std::vector<uint16_t> kinectFrame(Robot::kinectResolution.x * Robot::kinectResolution.y);
		robot->OnFrame([&](uint16_t* frame, int32_t frameSize, glm::vec3 position, float angle)->void {
			kinectFrameMutex.lock();
			memcpy_s(kinectFrame.data(),frameSize, frame,frameSize);
			newFrame = true;
			kinectFrameMutex.unlock();
		});
		// Main loop
		while (window.IsOpen())
		{
			//Update rendering info info
			info.view = camera.CalculateView();
			info.camera = camera.GetPosition();
			{
				const auto now = std::chrono::high_resolution_clock::now();


				if (now - lastFpsMeasure > std::chrono::seconds(1))
				{
					fps = fpsCount;
					fpsCount = 0;
					lastFpsMeasure = now;
				}
				else fpsCount++;

				info.elapsed = static_cast<float>((now - lastframe).count()) / 1000000000.f;
				lastframe = now;
			}

			// Process new frame in render thread
			if (newFrame)
			{
				kinectFrameMutex.lock();
				preview->Set(kinectFrame.data(), Robot::kinectResolution);
				map->Merge(glm::vec3(glm::vec2(robot->GetPosition()), 0), glm::eulerAngles(robot->GetOrientation()).z - glm::half_pi<float>(), reinterpret_cast<short*>(kinectFrame.data()), Robot::kinectResolution);
				newFrame = false;
				kinectFrameMutex.unlock();

			}

			hud.Update(*robot,fps);

			//Camera controls
			const float s = cameraSpeed * (window.IsKeyPressed(GLFW_KEY_F) ? 20.f : 1.f);
			camera.Move({
				(window.IsKeyPressed(GLFW_KEY_W) ? 1.f : (window.IsKeyPressed(GLFW_KEY_S) ? -1.f : 0.f)) * s * info.elapsed,
				(window.IsKeyPressed(GLFW_KEY_A) ? 1.f : (window.IsKeyPressed(GLFW_KEY_D) ? -1.f : 0.f)) * s * info.elapsed,
				(window.IsKeyPressed(GLFW_KEY_SPACE) ? 1.f : (window.IsKeyPressed(GLFW_KEY_LEFT_SHIFT) ? -1.f : 0.f)) * s * info.elapsed,
				});

			camera.Turn({
				0,0,
				(window.IsKeyPressed(GLFW_KEY_Q) ? 1.f : (window.IsKeyPressed(GLFW_KEY_E) ? -1.f : 0.f)) * cameraSensitivity * info.elapsed * 200.f
				});


			if constexpr (manualControl)
			{
				robot->ForwardBackward((window.IsKeyPressed(GLFW_KEY_UP) ? 1.f : (window.IsKeyPressed(GLFW_KEY_DOWN) ? -1.f : 0.f)) * info.elapsed * 800.f);
				robot->Turn((window.IsKeyPressed(GLFW_KEY_LEFT) ? 1.f : (window.IsKeyPressed(GLFW_KEY_RIGHT) ? -1.f : 0.f)) * info.elapsed * glm::pi<float>() / 3.f);
			}


			// Update cursor
			if (lastPos.x > 0 && lastPos.y > 0 && lastPos.x < resolution.x && lastPos.y < resolution.y)
			{
				const auto cursorVector = camera.CalcWorldVector({ lastPos.x,resolution.y - lastPos.y },resolution, info.view, info.projection);
				const auto cursor = WorldCursor::IntersectRayWithPlane(glm::vec3(0, 0, 1), 0.f, camera.GetPosition(), cursorVector);
				if (cursor.has_value())
				{
					wc->SetPosition(cursor.value());
				}

				wc->SetVisibility(cursor.has_value() && !drag);

				SetCursor(LoadCursor(NULL, drag ? IDC_SIZEALL : ( cursor.has_value() ? IDC_ARROW : IDC_NO )));
			}
			else
				wc->SetVisibility(false);

			// Draw
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


			em.RenderAll(info);
			TempEntity::RenderAll(info);
			hud.Render();
			preview->Render(info);
			window.Display();
		}
		return EXIT_SUCCESS;
	}

};