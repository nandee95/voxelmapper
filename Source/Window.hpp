#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec2.hpp>
#include <functional>
#include <iostream>
#include <functional>
#include <map>
#include <vector>

#define Error(message) std::cout << "Error: "<<  std::string(message) << std::endl;
#define Debug(message) std::cout << "Debug: "<<  std::string(message) << std::endl;

template<typename T, typename P>
class Events
{
protected:
	P params;
	int32_t eventCount = 0;
	std::map<T, std::map<int32_t, std::function<void(const P&)>>> events;
public:
	Events()
	{

	}

	int32_t RegisterEvent(const T type, std::function<void(const P&)> func)
	{
		if (events.find(type) == events.end())
			events.insert(std::make_pair(type, std::map<int32_t, std::function<void(const P&)>>()));

		events[type].insert(std::make_pair(eventCount++, func));
		return eventCount - 1;
	}

	int32_t UnRegisterEvent(int32_t id)
	{
		for (auto& t : events)
		{
			auto it = t.second.find(id);
			if (it == t.second.end()) continue;
			t.second.erase(it);
		}
	}


	int8_t TriggerEvent(const T type)
	{
		if (events.find(type) == events.end()) return 0;
		int8_t count = 0;
		for (auto& e : events[type])
		{
			e.second(params);
			count++;
		}
		return count;
	}
};

enum WindowEventType
{
	Event_MouseMove,
	Event_Key,
	Event_MouseClick
};

struct WindowEventParams
{
	glm::vec2 position;
	int32_t action, mods, key;
};

class Window : public Events<WindowEventType, WindowEventParams>
{
protected:
	GLFWwindow* window;
	std::string title;
	bool fullscreen = false;
	glm::ivec2 size;

	static void glfwErrorCallback(int code, const char* message)
	{
		Error(message);
	}
public:
	Window(const glm::ivec2 size, const std::string title, bool debug) : title(title),size(size)
	{
		if (glfwInit() != GL_TRUE)
		{
			Error("Failed to initialize GLFW.");
			return;
		}
		glfwSetErrorCallback(glfwErrorCallback);

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
		//glfwWindowHint(GLFW_REFRESH_RATE, 60);

		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, debug ? GLFW_TRUE : GLFW_FALSE);


		window = glfwCreateWindow(size.x, size.y, title.c_str(), NULL, NULL);
		if (window == NULL)
		{
			glfwTerminate();
			Error("Failed to initialize window");
			return;
		}

		glfwSetWindowUserPointer(window, this);


		glfwMakeContextCurrent(window);
		glewInit();
		glEnable(GL_MULTISAMPLE);

		if (debug)
		{
			glEnable(GL_DEBUG_OUTPUT);
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageCallback(glDebugOutput, this);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
		}

		glfwSetKeyCallback(window, KeyCallback);
		glfwSetCursorPosCallback(window, CursorPosCallback);
		glfwSetMouseButtonCallback(window, MouseButtonCallback);
	}

	~Window()
	{
		glfwTerminate();
	}

	void ToggleFullscreen()
	{
		fullscreen = !fullscreen;
		auto* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);
		if (fullscreen)
		{
			glfwWindowHint(GLFW_RED_BITS, mode->redBits);
			glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
			glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);

			glfwSetWindowMonitor(window,monitor,0,0, mode->width, mode->height, 0);
		}
		else {
			glfwSetWindowMonitor(window, NULL, mode->width / 2 - size.x / 2 ,mode->height / 2 - size.y / 2, size.x,size.y, 0);
		}
	}

	inline GLFWwindow* GetHandle()
	{
		return window;
	}

	inline glm::ivec2 GetSize()
	{
		glm::ivec2 result;
		glfwGetWindowSize(window, &result.x, &result.y);
		return result;
	}

	inline void SetCursorPos(const glm::vec2 pos)
	{
		glfwSetCursorPos(window, pos.x, pos.y);
	}

	inline float GetAspect()
	{
		const auto s = GetSize();
		return (float)s.y / s.x;
	}

	inline bool IsKeyPressed(int32_t key) const
	{
		return glfwGetKey(window, key) == GLFW_PRESS;
	}

	inline bool IsOpen() const
	{
		return !glfwWindowShouldClose(window);
	}

	inline void Close()
	{
		glfwSetWindowShouldClose(window, true);
	}

	inline void SetCursorVisible(const bool& state)
	{
		glfwSetInputMode(window, GLFW_CURSOR, state ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_HIDDEN);
	}

	inline void Display()
	{
		glfwSwapInterval(1);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	inline GLFWwindow* Handle() const
	{
		return window;
	}

	inline const glm::vec2 GetCursorPos() const
	{
		double pos[2];
		glfwGetCursorPos(window, &pos[0], &pos[1]);
		return { pos[0],pos[1] };
	}

protected:

	static void APIENTRY glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
	{
		if (id == 131169 || id == 131185 || id == 131218 || id == 131204 || id == 131154 || id == GL_INVALID_OPERATION) return;

		Debug(std::string(message));
	}

	static void KeyCallback(GLFWwindow* window, int32_t key, int32_t scancode, int32_t action, int32_t mods)
	{
		Window* that = (Window*)glfwGetWindowUserPointer(window);

		that->params.key = key;
		that->params.action = action;
		that->params.mods = mods;
		that->TriggerEvent(Event_Key);
	}

	static void CursorPosCallback(GLFWwindow* window, double posx, double posy)
	{
		Window* that = (Window*)glfwGetWindowUserPointer(window);

		that->params.position = { posx,posy };
		that->TriggerEvent(Event_MouseMove);
	}

	static void MouseButtonCallback(GLFWwindow* window, int32_t button, int32_t action, int32_t mods)
	{
		Window* that = (Window*)glfwGetWindowUserPointer(window);

		double pos[2];
		glfwGetCursorPos(window, &pos[0], &pos[1]);
		that->params.position.x = pos[0];
		that->params.position.y = pos[1];
		that->params.action = action;
		that->params.mods = mods;
		that->params.key = button;
		that->TriggerEvent(Event_MouseClick);
	}
};