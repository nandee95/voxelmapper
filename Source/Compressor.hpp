#pragma once
#include <zlib.h>
#include <vector>
#include <stdint.h>
#include <assert.h>

class Compressor
{
    public:
        static std::vector<uint8_t> Compress(void* data, const size_t size)
        {

            const size_t BUFSIZE = 128 * 1024;
            uint8_t temp_buffer[BUFSIZE];

            z_stream strm;
            strm.zalloc = 0;
            strm.zfree = 0;
            strm.next_in = reinterpret_cast<uint8_t*>(data);
            strm.avail_in = size;
            strm.next_out = temp_buffer;
            strm.avail_out = BUFSIZE;

            deflateInit(&strm, Z_BEST_COMPRESSION);

            std::vector<uint8_t> buffer;
            while (strm.avail_in != 0)
            {
                int res = deflate(&strm, Z_NO_FLUSH);
                assert(res == Z_OK);
                if (strm.avail_out == 0)
                {
                    buffer.insert(buffer.end(), temp_buffer, temp_buffer + BUFSIZE);
                    strm.next_out = temp_buffer;
                    strm.avail_out = BUFSIZE;
                }
            }

            int deflate_res = Z_OK;
            while (deflate_res == Z_OK)
            {
                if (strm.avail_out == 0)
                {
                    buffer.insert(buffer.end(), temp_buffer, temp_buffer + BUFSIZE);
                    strm.next_out = temp_buffer;
                    strm.avail_out = BUFSIZE;
                }
                deflate_res = deflate(&strm, Z_FINISH);
            }

            assert(deflate_res == Z_STREAM_END);
            buffer.insert(buffer.end(), temp_buffer, temp_buffer + BUFSIZE - strm.avail_out);
            deflateEnd(&strm);

            return buffer;
        }


        static std::vector<uint8_t> Compress2(void* data, const size_t size)
        {
            std::vector<uint8_t> buffer;
            buffer.resize(size);
            uLongf outSize;
            compress(reinterpret_cast<Bytef*>(buffer.data()), &outSize, reinterpret_cast<Bytef*>(data), size);
            buffer.resize(outSize);
            return buffer;
        }

    static std::vector<uint8_t> Decompress(void* data,const size_t size,const size_t destlen)
    {
        std::vector<uint8_t> buffer;
        buffer.resize(destlen);
        uLongf destsize = destlen;
        uncompress(buffer.data(), &destsize, reinterpret_cast<uint8_t*>(data), size);
        return buffer;
    }
};