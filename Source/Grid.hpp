#pragma once

#include <stdint.h>
#include "VertexArray.hpp"
#include "Shader.hpp"

#include "Entity.hpp"

class Grid : public Entity
{
	VertexArray va;
	std::shared_ptr<Shader> shader;
public:
	virtual void LoadResources() override
	{
		shader = shader->LoadFromMemory({
			"Grid",
			GLSL(
#ifdef GLSL_VERT\n
layout(location = 0) in vec3 aPos; \n
layout(location = 1) uniform mat4 projection; \n
layout(location = 2) uniform mat4 view; \n
layout(location = 3) uniform vec3 camera; \n
void main() {
\n
gl_Position = projection * view * vec4(aPos + vec3(floor(camera.x / 1000.0) * 1000.0,floor(camera.y / 1000.0) * 1000.0,0), 1.0); \n
}\n
#elif defined GLSL_FRAG\n
layout(location = 0) out vec4 FragColor;\n
void main()\n
{ \n
	float dist = gl_FragCoord.z / gl_FragCoord.w; \n
	float v = 1 - dist / 30000.0; \n
	FragColor = vec4(0,0.6,1,v); \n
}\n
#endif
			)
			});
		float* pts=new float[101 * 2 * 2 * 3];
		size_t cnt = 0;
		constexpr float spacing = 1000;
		for (int8_t x = -50; x <= 50; x++)
		{
			pts[cnt++] = x * spacing;	pts[cnt++] = 50 * spacing;	pts[cnt++] = 0;
			pts[cnt++] = x * spacing;	pts[cnt++] = -50 * spacing;	pts[cnt++] = 0;
		}

		for (int8_t y = -50; y <= 50; y++)
		{
			pts[cnt++] = 50 * spacing;	pts[cnt++] = y * spacing;	pts[cnt++] = 0;
			pts[cnt++] = -50 * spacing;	pts[cnt++] = y * spacing;	pts[cnt++] = 0;
		}

		va.FillVertices(pts, cnt * sizeof(float));
		va.SetAttribPointers({ {3,GL_FLOAT} });
		delete pts;
	}

	virtual void Render(const FrameInfo& info)
	{
		shader->Use();
		shader->SetUniform(1, info.projection);
		shader->SetUniform(2, info.view);
		shader->SetUniform(3, info.camera);
		va.DrawArrays(GL_LINES, 0, 101 * 2 * 2 * 3);
	}

};