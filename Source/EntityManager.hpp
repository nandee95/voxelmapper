#pragma once
#include "Entity.hpp"
#include <vector>
#include <memory>

class EntityManager
{
protected:
	std::vector<std::shared_ptr<Entity>> entities;
public:
	void Add(std::shared_ptr<Entity> entity)
	{
		entity->LoadResources();
		entities.push_back(entity);
	}

	void RenderAll(const FrameInfo& info)
	{
		for (const auto& e : entities)
		{
			e->Render(info);
		}
	}
};