#pragma once

#include "Text.hpp"
#include "VertexArray.hpp"
#include "Shader.hpp"
#include "Robot.hpp"
#include <sstream>
#include <bitset>

class Hud
{
	float aspect = 1.f;
	Text statusText;
public:
	void LoadResources(const glm::vec2& resolution) {


	}

	Hud()
	{
		statusText.SetPosition({ 20,20 });
	}

	void Update(const Robot& robot,const int32_t& fps)
	{
		std::stringstream ss;
		ss << "Robot status:" << std::endl;
		ss << "            " << (robot.IsConnected() ? "Connected" : "Disconnected") << std::endl;
		const auto pos = robot.GetPosition();
		ss << "Rendering:" << std::endl;
		ss << "            FPS: " << fps << std::endl;
		ss << "Kinect:" << std::endl;
		ss << "            FPS: " << static_cast<int32_t>(robot.GetFps()) << std::endl;
		ss << "            RES: " << static_cast<int32_t>(Robot::kinectResolution.x) << "x" << static_cast<int32_t>(Robot::kinectResolution.y) << std::endl;
		ss << "Robot position:" << std::endl;
		ss << "            X: " << static_cast<int32_t>(pos.x) << " mm" << std::endl;
		ss << "            Y: " << static_cast<int32_t>(pos.y) << " mm" << std::endl;
		ss << "            Z: " << static_cast<int32_t>(pos.z) << " mm" << std::endl;
		ss << "Robot angle:" << std::endl;
		ss << "            " << (static_cast<int32_t>(glm::degrees(glm::eulerAngles(robot.GetOrientation()).z)) + 180) << " degrees" << std::endl;
		/*ss << "Proxity Optical:" <<  std::endl;
		ss << "            Bits: " << std::bitset<4>(0b0101) << std::endl;
		ss << "Proxity Ultrasonic:" << std::endl;
		ss << "            Left: " << std::nan << " mm" << std::endl;
		ss << "            Center: " << std::nan << " mm" << std::endl;
		ss << "            Right: " << std::nan << " mm" << std::endl;*/

		statusText.SetText(ss.str());
	}

	void Render()
	{
		statusText.Render();
	}

};