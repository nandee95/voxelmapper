#pragma once
#include <string>
#include <array>

#if __has_include(<glm/glm.hpp>)
	#define USE_GLM
	#include <glm/glm.hpp>
	#include <glm/gtc/type_ptr.hpp>
	#include <glm/gtc/matrix_transform.hpp>
#endif

#define GLSL_VERSION 430
#define GLSL(SRC)  #SRC

#ifdef USE_SPIRV
#error "SPIR-V Shaders not implemented yet!"
#endif

static struct ShaderData
{
	char* Name;
	char* Source;
};

class Shader
{
protected:
#ifndef USE_SPIRV
	static constexpr struct {
		GLuint ShaderType;
		char* ShaderDefine;
	} ShaderTypes[] = {
		{GL_VERTEX_SHADER,"GLSL_VERT"},
		{GL_FRAGMENT_SHADER,"GLSL_FRAG"}
	};
#endif
public:
	GLuint program;

	Shader(const GLuint& id) : program(id) {}
	~Shader()
	{
		glDeleteProgram(program);
	}

	static std::shared_ptr<Shader> LoadFromMemory(const ShaderData& data)
	{
		GLuint program = glCreateProgram();
		glObjectLabel(GL_PROGRAM, program, strlen(data.Name), data.Name);

		if (!program) return {};

		for (const auto& type : ShaderTypes)
		{
			//Create shader
			GLuint shader = glCreateShader(type.ShaderType);
			if (!shader)
			{
				Error("Failed to create shader.");
				glDeleteProgram(program);
				return {};
			}
			glObjectLabel(GL_SHADER, shader, strlen(data.Name), data.Name);

			const std::string source = "#version " + std::to_string(GLSL_VERSION) + "\n#define " + type.ShaderDefine + "\n" + std::string(data.Source);

			glShaderSource(shader, 1, &std::array<const char*, 1> { source.c_str() }[0], NULL);
			glCompileShader(shader);

			//Check for errors
			GLint success = 0;
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
			if (!success)
			{
				char infoLog[512];
				glGetShaderInfoLog(shader, 512, NULL, infoLog);
				std::cout << "ERROR " << type.ShaderDefine << " : " << infoLog << std::endl;
				glDeleteProgram(program);
				glDeleteShader(shader);
				return {};
			}
			glAttachShader(program, shader);
			glDeleteShader(shader);
		}
		
		glLinkProgram(program);

		//Check for errors
		GLint success = 0;
		glGetProgramiv(program, GL_LINK_STATUS, &success);
		if (!success)
		{
			glDeleteProgram(program);
			return {};
		}

		return std::make_shared<Shader>(program);
	}

	static const uint64_t HashFromMemory(const ShaderData& data)
	{
		return std::hash<std::string>{}(data.Source);
	}

	inline void Use() const noexcept
	{
		glUseProgram(program);
	}

	inline const GLint GetAttribLocation(const std::string& attrib) const
	{
		return glGetAttribLocation(program, attrib.c_str());
	}

	inline const GLint GetUniformLocation(const std::string& attrib) const
	{
		return glGetUniformLocation(program, attrib.c_str());
	}

	template <typename T>
	inline void SetUniform(const GLint& location, const T& value)
	{
		if constexpr (std::is_same<T, GLint>())						glUniform1i(location, value);
		else if constexpr (std::is_same<T, GLuint>())				glUniform1ui(location, value);
		else if constexpr (std::is_same<T, GLfloat>())				glUniform1f(location, value);
		else if constexpr (std::is_same<T, GLdouble>())				glUniform1d(location, value);
#ifdef USE_GLM
		else if constexpr (std::is_same<T, glm::vec2>())			glUniform2f(location, value.x, value.y);
		else if constexpr (std::is_same<T, glm::vec3>())			glUniform3f(location, value.x, value.y, value.z);
		else if constexpr (std::is_same<T, glm::vec4>())			glUniform4f(location, value.x, value.y, value.z, value.w);
		else if constexpr (std::is_same<T, glm::mat2>())			glUniformMatrix2fv(location, 1, GL_FALSE, glm::value_ptr(value));
		else if constexpr (std::is_same<T, glm::mat3>())			glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(value));
		else if constexpr (std::is_same<T, glm::mat4>())			glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
#endif
		else static_assert("Unsupported uniform type");
	}

};