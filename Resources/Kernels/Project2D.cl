#include "KernelInfo.cl"

#define INSPECTED_LAYERS 5
#define RADIUS 4

void kernel Project2D(
    const global struct KernelInfo* info,
    global char* plane,
    global int* neighbourBits,
	global char* chunk00,
	global char* chunk01,
	global char* chunk02,
	global char* chunk03,
	global char* chunk04,
	global char* chunk05,
	global char* chunk06,
	global char* chunk07,
	global char* chunk08
)
{
    global char* chunks[3][3];
	chunks[0][0] = chunk00;
	chunks[0][1] = chunk01;
	chunks[0][2] = chunk02;
	chunks[1][0] = chunk03;
	chunks[1][1] = chunk04;
	chunks[1][2] = chunk05;
	chunks[2][0] = chunk06;
	chunks[2][1] = chunk07;
	chunks[2][2] = chunk08;

    const int id = get_global_id(0);
    const int x = id % (int)info->gridSize.x;
    const int y = id / (int)info->gridSize.x;

    plane[id] = 0;

    int i=0;
    for(int3 offset=(int3)(0,-RADIUS,0);offset.y<RADIUS;offset.y++)
    {
        for(offset.x = -RADIUS ; offset.x < RADIUS ;offset.x++)
        {
            for(offset.z = 0;offset.z < INSPECTED_LAYERS; offset.z++)
            {
                int3 globalPos = (int3)(x,y,0) + offset;
                int3 chunkOfs = convert_int3(floor(convert_float3(globalPos) / info->gridSize));
                int3 localPos = globalPos - chunkOfs;
                if(*neighbourBits>>i & 1 && chunks[-chunkOfs.y+1][-chunkOfs.x+1][CHUNK_ID_VEC(localPos)] != 0 && length(convert_float3(offset)) < RADIUS)
                {
                    plane[id] = 1;
                    return;
                }
            }
            i++;
        }
    }
}