#include "KernelInfo.cl"

void kernel depthfieldmerger(
	const global struct KernelInfo* info,
	const global float3* position,
	const global int* field,
	global int* moddedChunks,
	global char* chunk00,
	global char* chunk01,
	global char* chunk02,
	global char* chunk03,
	global char* chunk04,
	global char* chunk05,
	global char* chunk06,
	global char* chunk07,
	global char* chunk08,
	global char* chunk09,
	global char* chunk10,
	global char* chunk11,
	global char* chunk12,
	global char* chunk13,
	global char* chunk14,
	global char* chunk15,
	global char* chunk16,
	global char* chunk17,
	global char* chunk18,
	global char* chunk19,
	global char* chunk20,
	global char* chunk21,
	global char* chunk22,
	global char* chunk23,
	global char* chunk24
)
{
	global char* chunks[5][5];
	chunks[0][0] = chunk00;
	chunks[0][1] = chunk01;
	chunks[0][2] = chunk02;
	chunks[0][3] = chunk03;
	chunks[0][4] = chunk04;
	chunks[1][0] = chunk05;
	chunks[1][1] = chunk06;
	chunks[1][2] = chunk07;
	chunks[1][3] = chunk08;
	chunks[1][4] = chunk09;
	chunks[2][0] = chunk11;
	chunks[2][1] = chunk11;
	chunks[2][2] = chunk12;
	chunks[2][3] = chunk13;
	chunks[2][4] = chunk14;
	chunks[3][0] = chunk15;
	chunks[3][1] = chunk16;
	chunks[3][2] = chunk17;
	chunks[3][3] = chunk18;
	chunks[3][4] = chunk19;
	chunks[4][0] = chunk20;
	chunks[4][1] = chunk21;
	chunks[4][2] = chunk22;
	chunks[4][3] = chunk23;
	chunks[4][4] = chunk24;

	const int id = get_global_id(0);
	const float3 fieldLocalSpace = (float3)(
		id % (int)info->fieldSize.x,
		(id / (int)info->fieldSize.x) % (int)info->fieldSize.y,
		id / (int)(info->fieldSize.x * info->fieldSize.y)
		);
	
	const float3 fieldSpace = floor(fieldLocalSpace - info->fieldCenter + floor(*position / info->gridResolution));

	float3 centerChunk = floor(*position / 1000);
	centerChunk.z = 0;

	const float3 chunkOffset = (floor(convert_float3(fieldSpace) / info->gridSize.x)) - centerChunk;
	if (chunkOffset.x < -2 || chunkOffset.y < -2 || chunkOffset.x > 2 || chunkOffset.y > 2) return;
	
	
	float3 localSpace = fmod(fieldSpace + (float3)(100000, 100000, 100000), info->gridSize.x);

	if(!CHUNK_IN_RANGE_VEC(localSpace)) return;

	switch (field[FIELD_ID_VEC(fieldLocalSpace)])
	{
	case 1:
		chunks[(int)chunkOffset.y + 2][(int)chunkOffset.x + 2][CHUNK_ID_VEC(localSpace)]=1;
		break;
	case -1:
		chunks[(int)chunkOffset.y + 2][(int)chunkOffset.x + 2][CHUNK_ID_VEC(localSpace)]=0;
		break;
	}

}