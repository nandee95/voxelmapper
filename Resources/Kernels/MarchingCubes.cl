#include "KernelInfo.cl"

//Vertex struct
struct __attribute__((packed)) vertex
{
    float3 pos, norm;
};
// Triangle struct (for safe insertion)
struct __attribute__((packed)) triangle
{
    struct vertex a, b, c;
};

void kernel marchingcubes(
    // Input
    const global struct KernelInfo* info,
    const global char* chunk,
    // Output
    global struct triangle* triangles,
    global int* vtc,
    //Neighbours
    const global int* nbits,
    const global char* nx,
    const global char* ny,
    const global char* nxy,
    const global float3 lookup[256][16]
 )
{ 
    const struct ivec3 neighbourTable[8] = {
        {0,0,0},
        {0,1,0},
        {1,1,0},
        {1,0,0},
        {0,0,1},
        {0,1,1},
        {1,1,1},
        {1,0,1},
    };

    // Calculation position
    const int id = get_global_id(0);

    const int z = id / ((int)info->gridSize.x * (int)info->gridSize.y);
    const int y = (id / (int)info->gridSize.x) % (int)info->gridSize.y;
    const int x = id % (int)info->gridSize.x;
    // Calculating configuration id
    int triId = 0;
    #pragma unroll
    for (int i = 0; i < 8; i++)
    {
        if (z + neighbourTable[i].z == info->gridSize.z) continue; // Z positive neighbour
        else if ((x + neighbourTable[i].x == info->gridSize.x && y + neighbourTable[i].y == info->gridSize.y)) // XY diagonal neighbour
        {
            if (!(*nbits & 0x4) || nxy[((z+ neighbourTable[i].z) * (int)info->gridSize.y * (int)info->gridSize.x)] != 1) continue;
        }
        else if (x + neighbourTable[i].x == info->gridSize.x) // X positive neighbour
        {
            if(!(*nbits & 0x1) || nx[((z + neighbourTable[i].z) * (int)info->gridSize.y * (int)info->gridSize.x) + ((y + neighbourTable[i].y) * (int)info->gridSize.x)] != 1) continue;
        }
        else if(y + neighbourTable[i].y == info->gridSize.y) // Y positive neighbour
        {
            if (!(*nbits & 0x2) || ny[((z + neighbourTable[i].z) * (int)info->gridSize.y * (int)info->gridSize.x) + (x + neighbourTable[i].x)]!=1) continue;
        }
        else if (chunk[((z + neighbourTable[i].z) * (int)info->gridSize.y * (int)info->gridSize.x) + ((y + neighbourTable[i].y) * (int)info->gridSize.x) + (x + neighbourTable[i].x)] != 1)
        {
            continue;
        }
        triId |= 1 << i;
    }
    // Pushing triangles
    struct triangle tri;
    const float3 base=(float3)(x,y,z);
    for (int i=0; lookup[triId][i].x != -1; )
    {
        tri.a.pos = base + lookup[triId][i++];
        tri.b.pos = base + lookup[triId][i++];
        tri.c.pos = base + lookup[triId][i++];
        
        tri.a.norm = tri.b.norm = tri.c.norm = cross(tri.b.pos - tri.a.pos,tri.c.pos - tri.a.pos);

        triangles[atomic_add(vtc, 1)]= tri;
    }
}