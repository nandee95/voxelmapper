#include "KernelInfo.cl"

void kernel depthmapprojection(
	const global struct KernelInfo* info,
	const global float* angle,
	// Depthmap
	const global short* depthMap,
	const global float2* depthMapRes,
	// Output field
	global int* field
)
{
	const int id = get_global_id(0) ;

	if (depthMap[id] == 0) return;
	
	const int x= depthMapRes->x - (id % (int)depthMapRes->x);
	const int y = depthMapRes->y - ( id / (int)depthMapRes->x);
	
	const float2 step = info->kinectFOV / *depthMapRes;
	float2 rayAngle = (float2)(-info->kinectFOV.x / 2.0 + x * step.x + *angle, -info->kinectFOV.y / 2.0 + y * step.y + 3.1415 / 24);


	float3 targetPos = normalize((float3)(
		cos(rayAngle.x) * cos(rayAngle.y),
		sin(rayAngle.x) * cos(rayAngle.y),
		sin(rayAngle.y)
		)) * (float)depthMap[id] / info->gridResolution + info->fieldCenter;

	bool outOfBounds = targetPos.x < 0 || targetPos.y < 0 || targetPos.z < 0 || targetPos.x >= info->fieldSize.x || targetPos.y >= info->fieldSize.y || targetPos.z >= info->fieldSize.z;
	
	// Bresenham's Algorithm
	targetPos = floor(targetPos);
	float3 beginPos = info->fieldCenter;
	float3 d = fabs(targetPos-beginPos);
	float3 s = (float3)(targetPos.x > beginPos.x ? 1 : -1, targetPos.y > beginPos.y ? 1 : -1, targetPos.z > beginPos.z ? 1 : -1);
	if (d.x >= d.y && d.x >= d.z) // X is the leading axis
	{
		float p1 = 2 * d.y - d.x;
		float p2 = 2 * d.z - d.x;
		while (beginPos.x != targetPos.x)
		{
			beginPos.x += s.x;
			if (p1 >= 0)
			{
				beginPos.y += s.y;
				p1 -= 2 * d.x;
			}
			if (p2 >= 0)
			{
				beginPos.z += s.z;
				p2 -= 2 * d.x;
			}
			p1 += 2 * d.y;
			p2 += 2 * d.z;
			if (beginPos.x < 0 || beginPos.y < 0 || beginPos.z < 0 || beginPos.x >= info->fieldSize.x || beginPos.y >= info->fieldSize.y || beginPos.z >= info->fieldSize.z)
				break;
			atomic_cmpxchg(&field[FIELD_ID_VEC(beginPos)], 0, -1);
		}
	}
	else if (d.y >= d.x && d.y >= d.z)
	{
		float p1 = 2 * d.x - d.y;
		float p2 = 2 * d.z - d.y;
		while (beginPos.y != targetPos.y)
		{
			beginPos.y += s.y;
			if (p1 >= 0)
			{
				beginPos.x += s.x;
				p1 -= 2 * d.y;
			}
			if (p2 >= 0)
			{
				beginPos.z += s.z;
				p2 -= 2 * d.y;
			}
			p1 += 2 * d.x;
			p2 += 2 * d.z;
			if (beginPos.x < 0 || beginPos.y < 0 || beginPos.z < 0 || beginPos.x >= info->fieldSize.x || beginPos.y >= info->fieldSize.y || beginPos.z >= info->fieldSize.z)
				break;
			atomic_cmpxchg(&field[FIELD_ID_VEC(beginPos)], 0, -1);
		}
	}
	else
	{
		float p1 = 2 * d.y - d.z;
		float p2 = 2 * d.x - d.z;
		while (beginPos.z != targetPos.z)
		{
			beginPos.z += s.z;
			if (p1 >= 0)
			{
				beginPos.y += s.y;
				p1 -= 2 * d.z;
			}
			if (p2 >= 0)
			{
				beginPos.x += s.x;
				p2 -= 2 * d.z;
			}
			p1 += 2 * d.y;
			p2 += 2 * d.x;
			if (beginPos.x < 0 || beginPos.y < 0 || beginPos.z < 0 || beginPos.x >= info->fieldSize.x || beginPos.y >= info->fieldSize.y || beginPos.z >= info->fieldSize.z)
				break;
			atomic_cmpxchg(&field[FIELD_ID_VEC(beginPos)], 0, -1);
		}
	}

	// Set target pos to be part of the map
	if(!outOfBounds)
		field[FIELD_ID_VEC(targetPos)] = 1;
}