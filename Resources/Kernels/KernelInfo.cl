// Kernel info struct
struct __attribute__((packed)) KernelInfo
{
	float gridResolution;
	float3 gridSize;
	float3 fieldSize;
	float3 fieldCenter;
	float2 kinectFOV;
	float3 kinectOffset;
};

// Indexing definitions
#define PLANE_ID(_x,_y) ARRAY_ID_2D(_x,_y,info->gridSize.x)
#define PLANE_IN_RANGE(_x,_y) IN_RANGE_2D(_x,_y,info->gridSize.x,info->gridSize.y)

#define CHUNK_ID(_x,_y,_z) ARRAY_ID_3D(_x,_y,_z,info->gridSize.x,info->gridSize.y)
#define CHUNK_ID_VEC(vec) CHUNK_ID((vec).x,(vec.y),(vec).z)
#define CHUNK_IN_RANGE(_x,_y,_z) IN_RANGE_3D(_x,_y,_z,info->gridSize.x,info->gridSize.y,info->gridSize.z)
#define CHUNK_IN_RANGE_VEC(vec) CHUNK_IN_RANGE((vec).x,(vec).y,(vec).z)

#define FIELD_ID(_x,_y,_z) ARRAY_ID_3D(_x,_y,_z,info->fieldSize.x,info->fieldSize.y)
#define FIELD_ID_VEC(vec) FIELD_ID((vec).x,(vec).y,(vec).z)
#define FIELD_IN_RANGE(x,y,z) IN_RANGE_3D(x,y,z,info->fieldSize.x,info->fieldSize.y,info->fieldSize.z)
#define FIELD_IN_RANGE_VEC(vec) FIELD_IN_RANGE((vec).x,(vec).y,(vec).z)


#define ARRAY_ID_2D(_x,_y,_xmax) ((int)(_y) * (int)(_xmax) + (int)(_x))
#define IN_RANGE_2D(_x,_y,_xmax,_ymax) ((_x) >=0 && (_y) >=0 && (_x) < (_xmax) && (_y) < (_ymax) )

#define ARRAY_ID_3D(_x,_y,_z,_xmax,_ymax) ((int)(_z) * (int)(_xmax) * (int)(_ymax) + (int)(_y) * (int)(_xmax) + (int)(_x))
#define IN_RANGE_3D(_x,_y,_z,_xmax,_ymax,_zmax) ((_x) >=0 && (_y) >=0 && (_z) >= 0 && (_x) < (_xmax) && (_y) < (_ymax) && (_z) < (_zmax))

// GLM structs
struct __attribute__((packed)) vec3
{
	float x, y, z;
};

struct __attribute__((packed)) ivec3
{
	int x, y, z;
};
