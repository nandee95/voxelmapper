file(INSTALL
            ${CMAKE_SOURCE_DIR}/glm/${CONFIG}/glm_static.lib
        DESTINATION
            ${CMAKE_INSTALL_PREFIX}/lib
)

file(INSTALL
            ${CMAKE_SOURCE_DIR}/glm/${CONFIG}/glm_shared.dll
        DESTINATION
            ${CMAKE_INSTALL_PREFIX}/bin
)


get_filename_component(SOURCE_DIR ${CMAKE_SOURCE_DIR} DIRECTORY)
set(SOURCE_DIR ${SOURCE_DIR}/Dependency_Glm)
file(INSTALL
            ${SOURCE_DIR}/glm
        DESTINATION
            ${CMAKE_INSTALL_PREFIX}/include
)